package util;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.awt.event.KeyEvent;

public class PopupHandle {

    public static void waitForElement(WebDriver driver, By element){
        WebDriverWait wait = new WebDriverWait(driver, 60);
        wait.until(ExpectedConditions.visibilityOfElementLocated(element));
    }
    public static void handleEnter() throws InterruptedException, AWTException {
        Robot rb =new Robot();
        rb.keyPress(KeyEvent.VK_ENTER);
        rb.keyRelease(KeyEvent.VK_ENTER);
        Thread.sleep(2000);
    }
    public static void handleEscape() throws InterruptedException, AWTException {
        Robot rb =new Robot();
        rb.keyPress(KeyEvent.VK_ESCAPE);
        rb.keyRelease(KeyEvent.VK_ESCAPE);
        Thread.sleep(2000);
    }
    public static void alertHandle(WebDriver driver, By element) throws InterruptedException {
        try {
            waitForElement(driver,element);
            driver.findElement(element).click();
            for (int i = 0; i <= 1; i++) {
                PopupHandle.handleEscape();
            }
        } catch (UnhandledAlertException | AWTException f) {
            try {
                Alert alert = driver.switchTo().alert();
                String alertText = alert.getText();
                System.out.println("Alert data: " + alertText);
                alert.accept();
            } catch (NoAlertPresentException e) {
                e.printStackTrace();
            }
        }
    }
}
