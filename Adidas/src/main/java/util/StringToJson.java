package util;


import com.google.gson.JsonObject;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class StringToJson {
    public static JSONObject convert(String toBeConverted) throws ParseException {
        JSONObject jsonpObject = null;
        Object obj ;
        JSONParser jsonParser = new JSONParser();
        obj = jsonParser.parse(toBeConverted);
        jsonpObject = (JSONObject) obj;
      return jsonpObject;
    }
}
