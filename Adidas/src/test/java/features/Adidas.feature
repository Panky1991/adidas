Feature: Demoblaze feature file

  Scenario: Demo Online Scenario
    Given I open demo site and click on laptops link
    When I search required items and click on Add to Cart link
    And I navigate to cart and delete item from cart
    And I click on Place Order link
    Then I am able to verify purchase id and amount