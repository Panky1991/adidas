Feature: Adidas Pet feature file

  Scenario: Adidas Get Pet Scenario
    Given GET Request URL
    When I send GET request for available pets
    Then I am able to verify response of GET request

  Scenario: Adidas Post Pet Scenario
    Given POST Request URL
    When I send POST request for available pets
    Then I am able to verify response of POST request
    And I update Pet status to Sold

  Scenario: Adidas Put Pet Scenario
    Given PUT Request URL
    When I send POST request for available pets
    When I send PUT request to change status to sold for available pets
    Then I am able to verify response of PUT request