package stepdef;

import io.cucumber.datatable.dependency.com.fasterxml.jackson.databind.util.JSONPObject;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.junit.Assert;
import util.StringToJson;

import java.util.List;

public class PetStepDef {
    String baseuri = null;
    String basepath=null ;
    String url = null;
    Response response;
    @Given("(.*) Request URL")
    public void requestURL(String requestType) {
        if(requestType.equalsIgnoreCase("GET")) {
            baseuri = "https://petstore.swagger.io/v2/";
            basepath = "pet/findByStatus?status=available";
        }
        if(requestType.equalsIgnoreCase("POST")) {
            baseuri = "https://petstore.swagger.io/v2/";
            basepath = "pet";
        }
        if(requestType.equalsIgnoreCase("PUT")) {
            baseuri = "https://petstore.swagger.io/v2/";
            basepath = "pet";
        }
    }

    @When("I send (.*) request for available pets")
    public void iSendGETRequest(String requestType) throws ParseException {
        RestAssured.baseURI=baseuri;
        RestAssured.basePath=basepath;
        url = RestAssured.baseURI + RestAssured.basePath;
        RequestSpecification request = RestAssured.given();
        if(requestType.equalsIgnoreCase("GET")) {
            response = request.get(url);
        }
        if(requestType.equalsIgnoreCase("POST")) {
            String body = "{\n" +
                    "    \"id\": 2,\n" +
                    "    \"category\": {\n" +
                    "        \"id\": 0,\n" +
                    "        \"name\": \"string\"\n" +
                    "    },\n" +
                    "    \"name\": \"doggie\",\n" +
                    "    \"photoUrls\": [\n" +
                    "        \"string\"\n" +
                    "    ],\n" +
                    "    \"tags\": [\n" +
                    "        {\n" +
                    "            \"id\": 0,\n" +
                    "            \"name\": \"string\"\n" +
                    "        }\n" +
                    "    ],\n" +
                    "    \"status\": \"available\"\n" +
                    "}";
            JSONObject jsonObject = StringToJson.convert(body);
            response = request.header("Content-Type","application/json").body(jsonObject).when().post(url);
        }
    }

    @Then("I am able to verify response of (.*) request")
    public void iAmAbleToVerifyResponse(String requestType) {
        int returnCode = response.getStatusCode();
        Assert.assertEquals(returnCode,200);
        if(requestType.equalsIgnoreCase("GET")) {
            String res = response.then().extract().asString();
            Object statusCount = response.getBody().jsonPath().get("status");
            System.out.println(statusCount.toString());
            String[] statusArray = statusCount.toString().split(",");
            System.out.println("Total Available Pets are " + statusArray.length);
        }
        if(requestType.equalsIgnoreCase("POST")) {
            Object statusCount = response.getBody().jsonPath().get("status");
            System.out.println("Status of new Pet after addition is : "+statusCount.toString());
            Object idCount = response.getBody().jsonPath().get("id");
            System.out.println("Id of new Pet is : "+idCount.toString());
            String[] idArray = idCount.toString().split(",");
            for(String newPet : idArray){
            if(newPet.equalsIgnoreCase("2")){
                System.out.println("New Pet added and ID of it is : "+newPet);
                break;
            }}
        }
        if(requestType.equalsIgnoreCase("PUT")) {
            Object statusCount = response.getBody().jsonPath().get("status");
            Object idCount = response.getBody().jsonPath().get("id");
            System.out.println("Status of Pet id "+idCount+" after update is : "+statusCount.toString());
        }
        System.out.println("Status Code is verified and it is : " + returnCode);
    }

    @And("I update Pet status to Sold")
    public void iUpdatePetStatusToSold() {
        Object statusCount = response.getBody().jsonPath().get("status");
        String Status = statusCount.toString();
        Status =  "sold";
        System.out.println("Status of new Pet after updation is : "+Status);
    }

    @When("I send PUT request to change status to sold for available pets")
    public void iSendPUTRequestToChangeStatusToSoldForAvailablePets() throws ParseException {
        RestAssured.baseURI=baseuri;
        RestAssured.basePath=basepath;
        url = RestAssured.baseURI + RestAssured.basePath;
        RequestSpecification request = RestAssured.given();
            String body = "{\n" +
                    "    \"id\": 2,\n" +
                    "    \"category\": {\n" +
                    "        \"id\": 0,\n" +
                    "        \"name\": \"string\"\n" +
                    "    },\n" +
                    "    \"name\": \"doggie\",\n" +
                    "    \"photoUrls\": [\n" +
                    "        \"string\"\n" +
                    "    ],\n" +
                    "    \"tags\": [\n" +
                    "        {\n" +
                    "            \"id\": 0,\n" +
                    "            \"name\": \"string\"\n" +
                    "        }\n" +
                    "    ],\n" +
                    "    \"status\": \"sold\"\n" +
                    "}";
            JSONObject jsonObject = StringToJson.convert(body);
            response = request.header("Content-Type","application/json").body(jsonObject).when().post(url);
        }
}
