package stepdef;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import util.PopupHandle;
import util.StringCompare;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.*;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class AdidasStepDef {
    String driverPath = "src//test//resources//chromedriver.exe";
    WebDriver driver;
    String productAmount = null;
    By laptopLink = By.linkText("Laptops");
    By sonyLaptop = By.linkText("Sony vaio i5");
    By dellLaptop = By.linkText("Dell i7 8gb");
    By addToCart = By.linkText("Add to cart");
    By homepage = By.id("nava");
    By cart = By.linkText("Cart");
    By delete = By.xpath("//tbody[@id='tbodyid']/tr[1]/td[4]/a[1]");
    By rows = By.xpath("//div[@class='table-responsive']/table/tbody[@id='tbodyid']/tr/td");
    By cols = By.xpath("//div[@class='table-responsive']/table/thead/tr/th");
    //By delete = By.xpath("//a[contains(text(),'Delete')]");
    By placeOrder = By.xpath("//button[contains(text(),'Place Order')]");
    By name = By.id("name");
    By country = By.id("country");
    By city = By.xpath("//input[@id='city']");
    By card = By.xpath("//input[@id='card']");
    By month = By.xpath("//input[@id='month']");
    By year = By.xpath("//input[@id='year']");
    By purchase = By.xpath("//button[contains(text(),'Purchase')]");
    By amount = By.xpath("//label[@id='totalm']");
    By okButton = By.xpath("//button[contains(text(),'OK')]");
    By validation = By.xpath("//body/div[10]/p[1]");

    public AdidasStepDef() throws AWTException {
        DesiredCapabilities dc = new DesiredCapabilities();
        dc.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);
        dc.setCapability(CapabilityType.UNHANDLED_PROMPT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);
        System.setProperty("webdriver.chrome.driver", driverPath);
        driver = new ChromeDriver(dc);
    }

    @Given("I open demo site and click on (.*) link")
    public void iClickOnLaptopLink(String itemType) {
        if(itemType.equalsIgnoreCase("laptops")) {
            driver.navigate().to("https://www.demoblaze.com/index.html");
            driver.manage().window().maximize();
            System.out.println("Title is " + driver.getTitle());
            driver.findElement(laptopLink).click();
        }
    }

    @When("I search required items and click on Add to Cart link")
    public void iSearchRequiredItemsAndClickOnAddToCartLink() throws InterruptedException, AWTException {
        PopupHandle.waitForElement(driver,sonyLaptop);
            driver.findElement(sonyLaptop).click();
            //Thread.sleep(5000);
            PopupHandle.alertHandle(driver,addToCart);
    }

    @And("I navigate to cart and delete item from cart")
    public void iNavigateToCartAndDeleteItemFromCart() throws InterruptedException, AWTException {
            driver.findElement(homepage).click();
            driver.findElement(laptopLink).click();
        PopupHandle.waitForElement(driver,dellLaptop);
                    driver.findElement(dellLaptop).click();
                    //Thread.sleep(5000);
                    PopupHandle.alertHandle(driver,addToCart);
        PopupHandle.waitForElement(driver,cart);
        driver.findElement(cart).click();
        Thread.sleep(5000);
        //PopupHandle.waitForElement(driver,delete);
        driver.findElement(delete).click();
    }

    @And("I click on Place Order link")
    public void iClickOnPlaceOrderLink() throws InterruptedException {
        Thread.sleep(5000);
        //PopupHandle.waitForElement(driver,placeOrder);
        driver.findElement(placeOrder).click();
        PopupHandle.waitForElement(driver,name);
        String productValue = driver.findElement(amount).getText();
        productAmount=productValue;
        driver.findElement(name).sendKeys("Adidas");
        driver.findElement(country).sendKeys("India");
        driver.findElement(city).sendKeys("Delhi");
        driver.findElement(card).sendKeys("1234567891234567");
        driver.findElement(month).sendKeys("10");
        driver.findElement(year).sendKeys("2020");
        driver.findElement(purchase).click();
    }

    @Then("I am able to verify purchase id and amount")
    public void iAmAbleToVerifyPurchaseAmount() throws InterruptedException, IOException {
        PopupHandle.waitForElement(driver,validation);
        if(driver.findElement(validation).isDisplayed())
        {
            String validationText = driver.findElement(validation).getText();
            System.out.println("Validation text is "+validationText);
            String id = validationText.substring(3,11);
            String amount = validationText.substring(19,23);
            System.out.println("Purchase id is "+id);
            System.out.println("Purchase amount is "+amount);
            int result = StringCompare.compare(productAmount,amount);
            if(result==0)
            {
                System.out.println("Product Amount verified");
            }
            driver.findElement(okButton).click();
        }
        driver.quit();
    }
}
